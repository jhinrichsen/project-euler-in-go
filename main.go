package main

import "fmt"

func mkc() chan int {
	return make(chan int, 100000)
}

func passMultiplesOfN(in, out chan int, n int) {
	for i := range in {
		if i%n == 0 {
			out <- i
		}
	}
	close(out)
}

func generate(out chan int, n int) {
	for i := 0; i < n; i++ {
		out <- i
	}
	close(out)
}

func euler1Concurrent(n int) (sum int) {
	g, c3, c5 := mkc(), mkc(), mkc()
	go generate(g, n)
	go passMultiplesOfN(g, c3, 3)
	go passMultiplesOfN(c3, c5, 5)
	for i := range c5 {
		// fmt.Printf("Adding %v\n", i)
		sum += i
	}
	return sum
}

func euler1PlainVanilla(n int) (sum int) {
	for i := 0; i < n; i++ {
		if (i%3 == 0) && (i%5 == 0) {
			sum += i
		}
	}
	return sum
}

func mult(m, n int) (sum int) {
	for i := m; i < n; i += m {
		sum += i
	}
	return
}

func euler1Multiples(n int) (sum int) {
	return mult(3, n) + mult(5, n) - mult(15, n)
}

// Euler1 returns the sum off all numbers up to n that are multiples of 3 and 5
func Euler1() int {
	return euler1Concurrent(1000)
}

// gauss returns the sum of all numbers from i to limit (not included).
func gauss(i, limit uint) uint {
	limit--
	odd := func(n uint) bool {
		return n%2 != 0
	}
	n := limit / i          // number of pairs
	last := (limit / i) * i // last element
	pair := i + last

	sum := (n / 2) * pair

	if odd(n) { // need to add one middle element
		sum += pair / 2
	}
	return sum
}

func main() {
	fmt.Println(Euler1())
}
