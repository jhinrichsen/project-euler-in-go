package main

import (
	"testing"
)

const N = 1000

func TestEuler1(t *testing.T) {
	want := 33165
	got := Euler1()
	if want != got {
		t.Fatalf("want %d but got %d\n", want, got)
	}
}

func BenchmarkEuler1PlainVanilla(b *testing.B) {
	for i := 0; i < b.N; i++ {
		euler1PlainVanilla(N)
	}
}

func BenchmarkEuler1Multiples(b *testing.B) {
	for i := 0; i < b.N; i++ {
		euler1Multiples(N)
	}
}

func BenchmarkEuler1Concurrent1(b *testing.B) {
	for i := 0; i < b.N; i++ {
		euler1Concurrent(N)
	}
}

func BenchmarkEuler1GaussFormula(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = gauss(3, N) + gauss(5, N) - gauss(15, N)
	}
}
