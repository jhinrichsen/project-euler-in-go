= Project Euler in Go

Implementations for https://projecteuler.net[Project Euler].

# Problem #1

Finally got tired of implemention O(n) solutions in Clojure, Haskell, Forth,
Javascript, Rust, ... so i put some thought into O(1).

[quote, ken]
If your code is not performant, there's a loop in it.
